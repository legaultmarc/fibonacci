#!/usr/bin/env python2.7

import cProfile
import argparse

SQRT5 = 5 ** 0.5
PHI = (1 + SQRT5) * 0.5

def main(parser, fibos):
    args = parser.parse_args()

    algo = fibos.get(args.algo)
    if args.n:
        if args.time:
            time_call(algo, args.n)
        else:
            print algo(args.n)

    else:
        if not args.max:
            print ("Either use the -n option to compute a specific "
                   "Fibonacci number, or user --min and --max to "
                   "compute all numbers in a given range.")

            print ("If no --min is given, then the range will "
                   "be from 0 to --max.")
            
            parser.print_help()
            exit(0)
        min_n = 1 if not args.min else args.min
        for i in xrange(min_n, args.max + 1):
            if args.time:
                time_call(algo, i)
            else:
                print algo(i)

    if args.time:
        print

def fibo_rec(n):
    if n <= 1:
        return n
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)

def fibo_rec_mem(n, i = 0, j = 1):
    """Recursive version using tail-recursion

    """
    if n <= 1:
        return j
    else:
        return fibo_rec_mem(n - 1, j, i + j)
    

def fibo_log(n):
    """This method uses interesting algorithmic strategies
       and is supposed to be O(log n) (as I was told in an
       algorithms class, if I remember correctly). 

    """

    i = 1
    j = 0
    k = 0
    h = 1
    while n > 0:
        if n % 2 != 0:
            t = j * h
            j = i * h + j * k + t
            i = i * k + t
        t = h ** 2
        h = 2 * k * h + t
        k = k ** 2 + t
        n = int( n / 2 )
    return j

def fibo_iter(n):
    """Iterative method that is supposed to run in O(n).

    This method uses the mathematical definition but uses
    iteration instead of recursion.

    """

    i = 1
    j = 0
    for k in xrange(n):
        j = i + j
        i = j - i
    return j

def fibo_lin(n):
    """This method uses Binet's formula, which is mathematically
       correct and very fast, but is not easily implemented
       because of overflow errors at the PHI ** n step.

       Also, floating number arithmetic seems to introduce
       errors when n > 70.
       (http://bosker.wordpress.com/2011/07/27/computing-fibonacci-numbers-using-binet%E2%80%99s-formula/)
       
    """

    if n == 0: return n
    return int(round(PHI ** n / SQRT5))

def time_call(f, *args, **kwargs):
    """Function that measures execution time of a given function f
       with args and kwargs as arguments.

    """
    code = "{0}(".format(f.__name__)
    if args:
        for i in xrange(len(args)):
            if i != 0:
                code += ", "
            if type(args[i]) is str:
                code += "\"{0}\"".format(args[i])
            else:
                code += "{0}".format(str(args[i]))

    if kwargs:
        base = ", " if args else ""
        i = 0
        for k, v in kwargs.iteritems():
            if i != 0:
                base = ", "
            if type(v) is str:
                code += "{0}{1}=\"{2}\"".format(base, k, v)
            else:
                code += "{0}{1}={2}".format(base, k, str(v))
            i += 1
            
    code += ")"
    cProfile.run(code)
        
if __name__ == '__main__':

    fibos = {"iterative":   fibo_iter, 
             "logtime":     fibo_log, 
             "linear":      fibo_lin,
             "recursive":   fibo_rec,
             "recmem":      fibo_rec_mem}

    desc = ("Computes fibonacci numbers in a given range "
            "with a given algorithm")
    try:
        parser = argparse.ArgumentParser(description=desc)

        parser.add_argument("-n", "--n",
                            type=int,
                            help=("Index to generate when computing "
                                  "a single value"),
                            required=False)

        parser.add_argument("-a", "--algo",
                            dest="algo",
                            type=str,
                            choices=fibos.keys(),
                            required=True,
                            help="The algorithm to use to compute fibo.")

        parser.add_argument("--min",
                            type=int,
                            required=False,
                            help="Minimum value when generating a range.")

        parser.add_argument("--max",
                            type=int,
                            required=False,
                            help="Minimum value when generating a range.")

        parser.add_argument("--time", "-t",
                            action="store_true",
                            default=False,
                            required=False,
                            help=("Either time the execution of the algorithm, "
                                  "or return the computed Fibonacci number."))

        main(parser, fibos)
    except KeyboardInterrupt:
        exit()
